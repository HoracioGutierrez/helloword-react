import './App.css';
import HelloWord from './components/HelloWord.jsx';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HelloWord></HelloWord>
      </header>
    </div>
  );
}

export default App;
